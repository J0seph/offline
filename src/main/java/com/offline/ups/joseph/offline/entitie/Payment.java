package com.offline.ups.joseph.offline.entitie;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Payment {
    private int id;
    private int pay;
    private int invoiceValue;
    private int invoiceNumber;
}

