package com.offline.ups.joseph.offline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfflineApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfflineApplication.class, args);
		System.out.println("Okay");
	}

}