package com.offline.ups.joseph.offline.route;

import javax.sql.DataSource;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.offline.ups.joseph.offline.processor.NoValidate;
import com.offline.ups.joseph.offline.processor.Repository;
import com.offline.ups.joseph.offline.processor.Validate;

@Component
public class CustomRoute extends RouteBuilder{

    
    @Autowired
	DataSource dataSource;

    public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
    

    @Override
    public void configure() throws Exception {
        
        from("sftp:localhost:22/upload/?noop=true&username=foo&password=pass")
        .startupOrder(1)
        .to("file:src/data/?noop=True&fileName=cardsclients.csv")
        .end();

        from("stream:file?fileName=./src/data/cardsclients.csv&scanStream=true&scanStreamDelay=1000")
        .startupOrder(2)
        .process(new Validate())
        .choice()
        .when(header("isValid").contains(true))
            .to("file:src/data/log/?noop=True&fileName=valid.log&fileExist=Append&appendChars=\\n")
            .process(new Repository())
            //.to("stream:out")
            .to("jdbc:dataSource")
        .end();

        from("stream:file?fileName=./src/data/cardsclients.csv&scanStream=true&scanStreamDelay=2000")
        .startupOrder(3)
        .process(new NoValidate())
        .choice()
        .when(header("isValid").contains(false))
            .to("file:src/data/log/?noop=True&fileName=failed.log&fileExist=Append&appendChars=\\n")
        .end();
    }
    
}
