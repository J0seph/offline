package com.offline.ups.joseph.offline.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import com.offline.ups.joseph.offline.entitie.Payment;

public class NoValidate implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        String line = (String)exchange.getIn().getBody();
        String[] fields = line.split(",");
        
        if ("ID".equals(fields[0])) {
            exchange.getIn().setHeader("Id", 0);
            exchange.getIn().setHeader("isValid", true);
            return;
        }
        int id = Integer.parseInt(fields[0]);
        StringBuffer failedPayment = new StringBuffer();
        
        for (int i=0; i<6; i++){
            int invoice = Integer.parseInt(fields[12 + i]);
            int pay = Integer.parseInt(fields[18 + i]);
            Boolean isValid = invoice > 0 && pay > 0;
            Payment payment = new Payment();
            payment.setId(id);
            payment.setInvoiceNumber(i+1);
            payment.setInvoiceValue(invoice);
            payment.setPay(pay);
            
            if (!isValid) failedPayment.append(payment).append(";");
        }
        exchange.getIn().setHeader("Id", id);
        exchange.getIn().setHeader("isValid", !(failedPayment.toString().length() > 0));
        exchange.getIn().setBody(failedPayment);
    }
}
