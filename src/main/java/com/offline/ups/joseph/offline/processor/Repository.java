 package com.offline.ups.joseph.offline.processor;

import java.util.UUID;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class Repository implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        String message = exchange.getIn().getBody().toString();
        String[] payments = message.split(";");
        String id = UUID.randomUUID().toString();
        StringBuilder insert = new StringBuilder();
        insert.append("Insert Into Payment (Id, Info) ");
        for (int i = 0; i < payments.length; i++ ){
            insert.append( "Select '" + id + "' As Id, '" + payments[0] + "' As Info ");
            if (i != payments.length -1) insert.append( "Union All ");
        }
        exchange.getIn().setBody(insert.toString());
    }
}
